//https://api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric&appid=1aa5437a4cc6cb9c7112238f40ba6a86


const key = "1aa5437a4cc6cb9c7112238f40ba6a86";
const getForecast = async (city) => {
	const base =  "https://api.openweathermap.org/data/2.5/forecast"
	const query = `?q=${city}&units=metric&appid=${key}`;
	
	const response = await fetch(base+query);
	
//	console.log(response);
//function getForecast(city){
//	const response = fetch("https://api.openweathermap.org/data/2.5/forecast?q="+city+"&appid=1aa5437a4cc6cb9c7112238f40ba6a86)
//}
	if(!response.ok)
		throw new Error("Status Code:" + response.status);
	
	const data = await response.json();
//	console.log(data);
	return data;
}

getForecast('Mumbai')
.then(data => console.log(data))
.catch(err => console.warn(err));

